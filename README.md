# Network slicing with programmable switches
This Git-Repository is attached to the thesis "Network slicing with programmable switches" which proposes an active queueing management (AQM) algorithm for network
traffic differentiation using the data plane programming language P4 and compares it to
state-of-the-art AQM’s such as CoDel. During congestion, the effect of the suggested
combination of priority queuing with congestion awareness and CoDel AQM is investigated
on a bottleneck network topology. Therefore a virtual test environment is developed, using
the network emulator Mininet. Network metrics such as latency, flow duration, packet loss,
and throughput will be assessed
# Network topology
![plot](./doc/Testbed.png)

In this topology three hosts h1, h2 and h3 on the sender side are linked to one P4 switch
s1 and three receivers r1, r2 and r3 are attached to P4 switch 2. Both
switches are linked to each other. This network architecture exemplifies a bottleneck topology. The overall maximum link bandwidth is set to 5 Mbps. Each switch has a maximum queue
depth meaning the amount of packets a queue can hold for each output port. This value is set to 10000. The queue rate defined by the number of packets processed by the queue in one second is modified to 500 pps due to the maximum
capacity

# Install

## Setup
1. Install the following required tools:
    * [Mininet](https://github.com/mininet/mininet) - including **make install**
    * [P4 compiler bm](https://github.com/p4lang/p4c-bm)
    * [P4 behavioral model](https://github.com/p4lang/behavioral-model)
    * [P4-Utils](https://nsg-ethz.github.io/p4-utils/usage.html)

2. and in advance the following packages:
    ```
    sudo apt-get install openvswitch-testcontroller python-tk iperf3 xterm
    python -mpip install matplotlib
    pip install scapy
    ```
    
3. Clone the repository in the same parental folder than the 'behavioral-model'.

### Install p4c
    
    curl -L "http://download.opensuse.org/repositories/home:/p4lang/xUbuntu_${VERSION_ID}/Release.key" | sudo apt-key add -
    sudo apt-get update
    sudo apt install p4lang-p4c
    

### Install BMv2
    
    sudo apt-get install -y automake cmake libgmp-dev \
        libpcap-dev libboost-dev libboost-test-dev libboost-program-options-dev \
        libboost-system-dev libboost-filesystem-dev libboost-thread-dev \
        libevent-dev libtool flex bison pkg-config g++ libssl-dev
    

    
    1. ./autogen.sh
    2. ./configure
    3. make
    4. [sudo] make install  # if you need to install bmv2
    

## Run

Execute following script to build up the test environment
    
    cd srcP4/Evaluation/1_Router_TopoSlicing
    sudo python3 network.py
    
In another terminal execute follwing commands to start the simulation
    
    cd srcP4/Evaluation/1_Router_TopoSlicing
    sudo python3 1_normal_traffic_generator.py
    

## Evaluate

After running one simulation scenario, results can be extracted from ```srcP4/Evaluation/1_Router_TopoSlicing/logfiles```
