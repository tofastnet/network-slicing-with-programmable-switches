table_set_default slice_forwarding drop

table_add slice_forwarding forward_slicing 10.0.1.1 10.0.2.1/32 => 1 00:00:0a:00:02:01 
table_add slice_forwarding forward_slicing 10.0.1.2 10.0.2.2/32 => 2 00:00:0a:00:02:02 
table_add slice_forwarding forward_slicing 10.0.1.3 10.0.2.3/32 => 3 00:00:0a:00:02:03

table_add slice_forwarding forward_slicing 10.0.2.1 10.0.1.0/24 => 4 00:00:00:00:01:02 
table_add slice_forwarding forward_slicing 10.0.2.2 10.0.1.0/24 => 4 00:00:00:00:01:02 
table_add slice_forwarding forward_slicing 10.0.2.3 10.0.1.0/24 => 4 00:00:00:00:01:02 

set_queue_rate 500 1             		//in pps
set_queue_depth 10000 1           		//in nb_pkts

set_queue_rate 500 2             		//in pps
set_queue_depth 10000 2           		//in nb_pkts

set_queue_rate 500 3             		//in pps
set_queue_depth 10000 3           		//in nb_pkts

set_queue_rate 500 4             		//in pps
set_queue_depth 10000 4           		//in nb_pkts


