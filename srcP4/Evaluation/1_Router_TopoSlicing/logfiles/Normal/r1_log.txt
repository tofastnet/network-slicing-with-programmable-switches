------------------------------------------------------------
Server listening on UDP port 5000 with pid 246318
Receiving 1470 byte datagrams
UDP buffer size:  208 KByte (default)
------------------------------------------------------------
[  6] local 10.0.2.1 port 5000 connected with 10.0.1.1 port 47118
[ ID] Interval            Transfer     Bandwidth        Jitter   Lost/Total  Latency avg/min/max/stdev PPS  NetPwr
[  6] 0.0000-1.0000 sec  64.6 KBytes   529 Kbits/sec   0.769 ms    0/   45 (0%)  1.388/ 0.452/ 3.361/ 1.145 ms   45 pps  47.66
[  6] 1.0000-2.0000 sec  63.2 KBytes   517 Kbits/sec   1.415 ms    0/   44 (0%)  2.049/ 0.458/ 5.019/ 1.506 ms   44 pps  31.57
[  6] 2.0000-3.0000 sec  64.6 KBytes   529 Kbits/sec   1.210 ms    0/   45 (0%)  1.524/ 0.438/ 5.268/ 1.520 ms   45 pps  43.41
[  6] 3.0000-4.0000 sec  64.6 KBytes   529 Kbits/sec   0.956 ms    0/   45 (0%)  1.283/ 0.448/ 4.891/ 1.165 ms   45 pps  51.57
[  6] 4.0000-5.0000 sec  63.2 KBytes   517 Kbits/sec   1.720 ms    0/   44 (0%)  1.789/ 0.421/ 4.469/ 1.461 ms   45 pps  36.15
[  6] 5.0000-6.0000 sec  64.6 KBytes   529 Kbits/sec   1.223 ms    0/   45 (0%)  1.424/ 0.427/ 5.115/ 1.447 ms   45 pps  46.44
[  6] 6.0000-7.0000 sec  64.6 KBytes   529 Kbits/sec   1.762 ms    0/   45 (0%)  1.966/ 0.370/ 5.111/ 1.573 ms   44 pps  33.65
[  6] 7.0000-8.0000 sec  63.2 KBytes   517 Kbits/sec   1.299 ms    0/   44 (0%)  1.540/ 0.393/ 5.699/ 1.483 ms   45 pps  41.99
[  6] 8.0000-9.0000 sec  64.6 KBytes   529 Kbits/sec   1.662 ms    0/   45 (0%)  1.640/ 0.425/ 4.932/ 1.484 ms   45 pps  40.33
[  6] 9.0000-10.0000 sec  63.2 KBytes   517 Kbits/sec   1.209 ms    0/   44 (0%)  1.972/ 0.438/ 5.196/ 1.536 ms   45 pps  32.81
[  6] 0.0000-10.0037 sec   640 KBytes   524 Kbits/sec   1.209 ms    0/  446 (0%)  1.629/ 0.370/ 5.699/ 1.449 ms   44 pps  40.23
