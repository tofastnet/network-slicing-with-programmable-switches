------------------------------------------------------------
Server listening on UDP port 5002 with pid 246324
Receiving 1470 byte datagrams
UDP buffer size:  208 KByte (default)
------------------------------------------------------------
[  6] local 10.0.2.3 port 5002 connected with 10.0.1.3 port 38497
[ ID] Interval            Transfer     Bandwidth        Jitter   Lost/Total  Latency avg/min/max/stdev PPS  NetPwr
[  6] 0.0000-1.0000 sec   385 KBytes  3.15 Mbits/sec   1.065 ms    0/  268 (0%)  1.844/ 0.370/ 6.421/ 1.266 ms  268 pps  213.66
[  6] 1.0000-2.0000 sec   383 KBytes  3.14 Mbits/sec   1.579 ms    0/  267 (0%)  1.987/ 0.360/ 5.729/ 1.359 ms  267 pps  197.56
[  6] 2.0000-3.0000 sec   385 KBytes  3.15 Mbits/sec   1.376 ms    0/  268 (0%)  1.953/ 0.348/ 5.210/ 1.302 ms  268 pps  201.77
[  6] 3.0000-4.0000 sec   385 KBytes  3.15 Mbits/sec   1.267 ms    0/  268 (0%)  2.043/ 0.384/ 5.684/ 1.292 ms  267 pps  192.86
[  6] 4.0000-5.0000 sec   383 KBytes  3.14 Mbits/sec   1.364 ms    0/  267 (0%)  2.001/ 0.369/ 5.861/ 1.411 ms  267 pps  196.19
[  6] 5.0000-6.0000 sec   385 KBytes  3.15 Mbits/sec   0.974 ms    0/  268 (0%)  1.881/ 0.382/ 5.794/ 1.302 ms  268 pps  209.46
[  6] 6.0000-7.0000 sec   383 KBytes  3.14 Mbits/sec   1.361 ms    0/  267 (0%)  1.741/ 0.376/ 5.140/ 1.215 ms  267 pps  225.45
[  6] 7.0000-8.0000 sec   383 KBytes  3.14 Mbits/sec   1.151 ms    0/  267 (0%)  1.983/ 0.400/ 5.197/ 1.340 ms  268 pps  197.95
[  6] 8.0000-9.0000 sec   383 KBytes  3.14 Mbits/sec   1.504 ms    0/  267 (0%)  2.006/ 0.399/ 6.164/ 1.356 ms  267 pps  195.63
[  6] 9.0000-10.0000 sec   386 KBytes  3.16 Mbits/sec   1.545 ms    0/  269 (0%)  1.886/ 0.383/ 5.192/ 1.288 ms  268 pps  209.67
[  6] 0.0000-10.0044 sec  3.75 MBytes  3.15 Mbits/sec   1.545 ms    0/ 2676 (0%)  1.924/ 0.348/ 6.421/ 1.315 ms  267 pps  204.33
