------------------------------------------------------------
Server listening on UDP port 5000 with pid 324488
Receiving 1470 byte datagrams
UDP buffer size:  208 KByte (default)
------------------------------------------------------------
[  6] local 10.0.2.1 port 5000 connected with 10.0.1.1 port 54043
[ ID] Interval            Transfer     Bandwidth        Jitter   Lost/Total  Latency avg/min/max/stdev PPS  NetPwr
[  6] 0.0000-1.0000 sec   198 KBytes  1.62 Mbits/sec   1.917 ms    0/  138 (0%) 115.453/ 0.638/230.711/66.661 ms  138 pps  1.76
[  6] 1.0000-2.0000 sec   201 KBytes  1.65 Mbits/sec   3.659 ms    0/  140 (0%) 347.430/229.705/460.229/66.424 ms  138 pps  0.59
[  6] 2.0000-3.0000 sec   233 KBytes  1.91 Mbits/sec   8.230 ms    0/  162 (0%) 503.245/451.495/541.269/22.172 ms  164 pps  0.47
[  6] 3.0000-4.0000 sec   264 KBytes  2.16 Mbits/sec   7.807 ms    0/  184 (0%) 511.606/476.486/544.186/18.827 ms  179 pps  0.53
[  6] 4.0000-5.0000 sec   263 KBytes  2.15 Mbits/sec   8.724 ms    0/  183 (0%) 511.402/474.647/541.277/18.813 ms  178 pps  0.53
[  6] 5.0000-6.0000 sec   248 KBytes  2.03 Mbits/sec   5.695 ms    0/  173 (0%) 512.696/477.437/542.457/18.591 ms  185 pps  0.50
[  6] 6.0000-7.0000 sec   248 KBytes  2.03 Mbits/sec   8.662 ms    0/  173 (0%) 510.334/474.850/540.333/19.077 ms  172 pps  0.50
[  6] 7.0000-8.0000 sec   263 KBytes  2.15 Mbits/sec   8.081 ms    0/  183 (0%) 511.945/476.600/542.249/18.735 ms  178 pps  0.53
[  6] 8.0000-9.0000 sec   263 KBytes  2.15 Mbits/sec   8.031 ms    0/  183 (0%) 512.551/476.396/541.943/18.514 ms  178 pps  0.52
[  6] 9.0000-10.0000 sec   253 KBytes  2.07 Mbits/sec   5.342 ms    0/  176 (0%) 511.262/475.189/542.250/18.483 ms  187 pps  0.51
[  6] 0.0000-10.5412 sec  2.50 MBytes  1.99 Mbits/sec   7.951 ms    0/ 1784 (0%) 464.671/ 0.638/544.186/114.511 ms  169 pps  0.54
