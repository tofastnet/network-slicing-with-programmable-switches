------------------------------------------------------------
Server listening on UDP port 5001 with pid 247630
Receiving 1470 byte datagrams
UDP buffer size:  208 KByte (default)
------------------------------------------------------------
[  6] local 10.0.2.2 port 5001 connected with 10.0.1.2 port 39007
[ ID] Interval            Transfer     Bandwidth        Jitter   Lost/Total  Latency avg/min/max/stdev PPS  NetPwr
[  6] 0.0000-1.0000 sec   129 KBytes  1.06 Mbits/sec   1.462 ms    0/   90 (0%)  1.619/ 0.422/ 4.669/ 1.296 ms   89 pps  81.72
[  6] 1.0000-2.0000 sec   128 KBytes  1.05 Mbits/sec   1.628 ms    0/   89 (0%)  2.572/ 0.457/ 6.589/ 1.543 ms   89 pps  50.87
[  6] 2.0000-3.0000 sec   128 KBytes  1.05 Mbits/sec   1.528 ms    0/   89 (0%)  2.110/ 0.447/ 6.126/ 1.314 ms   89 pps  62.02
[  6] 3.0000-4.0000 sec   128 KBytes  1.05 Mbits/sec   1.874 ms    0/   89 (0%)  2.129/ 0.438/ 6.126/ 1.356 ms   89 pps  61.44
[  6] 4.0000-5.0000 sec   128 KBytes  1.05 Mbits/sec   1.337 ms    0/   89 (0%)  2.412/ 0.426/ 5.951/ 1.415 ms   89 pps  54.24
[  6] 5.0000-6.0000 sec   129 KBytes  1.06 Mbits/sec   1.659 ms    0/   90 (0%)  1.896/ 0.450/ 4.899/ 1.335 ms   89 pps  69.78
[  6] 6.0000-7.0000 sec   128 KBytes  1.05 Mbits/sec   1.141 ms    0/   89 (0%)  2.212/ 0.424/ 6.841/ 1.394 ms   89 pps  59.14
[  6] 7.0000-8.0000 sec   128 KBytes  1.05 Mbits/sec   1.542 ms    0/   89 (0%)  2.050/ 0.438/ 4.978/ 1.219 ms   89 pps  63.83
[  6] 8.0000-9.0000 sec   128 KBytes  1.05 Mbits/sec   1.736 ms    0/   89 (0%)  2.239/ 0.437/ 4.821/ 1.304 ms   89 pps  58.43
[  6] 9.0000-10.0000 sec   128 KBytes  1.05 Mbits/sec   1.677 ms    0/   89 (0%)  2.029/ 0.452/ 5.514/ 1.382 ms   89 pps  64.47
[  6] 0.0000-10.0017 sec  1.25 MBytes  1.05 Mbits/sec   1.677 ms    0/  892 (0%)  2.107/ 0.422/ 6.841/ 1.374 ms   89 pps  62.23
