------------------------------------------------------------
Server listening on UDP port 5002 with pid 324313
Receiving 1470 byte datagrams
UDP buffer size:  208 KByte (default)
------------------------------------------------------------
[  6] local 10.0.2.3 port 5002 connected with 10.0.1.3 port 33890
[ ID] Interval            Transfer     Bandwidth        Jitter   Lost/Total  Latency avg/min/max/stdev PPS  NetPwr
[  6] 0.0000-1.0000 sec   385 KBytes  3.15 Mbits/sec   1.227 ms    0/  268 (0%)  1.378/ 0.431/ 4.376/ 1.131 ms  267 pps  285.88
[  6] 1.0000-2.0000 sec   383 KBytes  3.14 Mbits/sec   1.225 ms    0/  267 (0%)  1.268/ 0.416/ 4.428/ 1.057 ms  267 pps  309.60
[  6] 2.0000-3.0000 sec   385 KBytes  3.15 Mbits/sec   1.232 ms    0/  268 (0%)  1.617/ 0.434/ 5.781/ 1.271 ms  268 pps  243.68
[  6] 3.0000-4.0000 sec   385 KBytes  3.15 Mbits/sec   0.533 ms    0/  268 (0%)  1.337/ 0.428/ 5.263/ 1.079 ms  267 pps  294.70
[  6] 4.0000-5.0000 sec   382 KBytes  3.13 Mbits/sec   1.191 ms    0/  266 (0%)  1.414/ 0.427/ 5.112/ 1.203 ms  267 pps  276.55
[  6] 5.0000-6.0000 sec   385 KBytes  3.15 Mbits/sec   1.334 ms    0/  268 (0%)  1.439/ 0.415/ 5.228/ 1.166 ms  268 pps  273.68
[  6] 6.0000-7.0000 sec   383 KBytes  3.14 Mbits/sec   1.505 ms    0/  267 (0%)  1.497/ 0.423/ 6.615/ 1.186 ms  267 pps  262.18
[  6] 7.0000-8.0000 sec   385 KBytes  3.15 Mbits/sec   1.251 ms    0/  268 (0%)  1.332/ 0.366/ 4.779/ 1.060 ms  268 pps  295.69
[  6] 8.0000-9.0000 sec   383 KBytes  3.14 Mbits/sec   1.272 ms    0/  267 (0%)  1.318/ 0.431/ 5.677/ 1.135 ms  267 pps  297.89
[  6] 9.0000-10.0000 sec   386 KBytes  3.16 Mbits/sec   1.208 ms    0/  269 (0%)  1.449/ 0.428/ 5.127/ 1.190 ms  267 pps  272.91
[  6] 0.0000-10.0069 sec  3.75 MBytes  3.14 Mbits/sec   1.208 ms    0/ 2676 (0%)  1.399/ 0.366/ 6.615/ 1.152 ms  267 pps  281.02
