------------------------------------------------------------
Client connecting to 10.0.2.3, UDP port 5002 with pid 401550
Sending 1470 byte datagrams, IPG target: 3738.40 us (kalman adjust)
UDP buffer size:  208 KByte (default)
------------------------------------------------------------
[  6] local 10.0.1.3 port 35648 connected with 10.0.2.3 port 5002
[ ID] Interval            Transfer     Bandwidth      Write/Err  PPS
[  6] 0.0000-1.0000 sec   386 KBytes  3.16 Mbits/sec  269/0      269 pps
[  6] 1.0000-2.0000 sec   385 KBytes  3.15 Mbits/sec  268/0      267 pps
[  6] 2.0000-3.0000 sec   383 KBytes  3.14 Mbits/sec  267/0      268 pps
[  6] 3.0000-4.0000 sec   383 KBytes  3.14 Mbits/sec  267/0      267 pps
[  6] 4.0000-5.0000 sec   385 KBytes  3.15 Mbits/sec  268/0      268 pps
[  6] 5.0000-6.0000 sec   383 KBytes  3.14 Mbits/sec  267/0      267 pps
[  6] 6.0000-7.0000 sec   385 KBytes  3.15 Mbits/sec  268/0      267 pps
[  6] 7.0000-8.0000 sec   383 KBytes  3.14 Mbits/sec  267/0      268 pps
[  6] 8.0000-9.0000 sec   385 KBytes  3.15 Mbits/sec  268/0      267 pps
[  6] 9.0000-10.0000 sec   383 KBytes  3.14 Mbits/sec  267/0      268 pps
[  6] 0.0000-10.0046 sec  3.75 MBytes  3.15 Mbits/sec  2676/0      267 pps
[  6] Sent 2676 datagrams
[  6] Server Report:
[  6] 0.0000-10.5074 sec  2.70 MBytes  2.15 Mbits/sec  32.096 ms  754/ 2678 (28%)  3.690/ 0.954/508.708/ 0.790 ms  254 pps  72.95
