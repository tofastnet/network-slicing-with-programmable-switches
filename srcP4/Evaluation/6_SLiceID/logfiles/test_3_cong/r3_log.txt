------------------------------------------------------------
Server listening on UDP port 5002 with pid 401507
Receiving 1470 byte datagrams
UDP buffer size:  208 KByte (default)
------------------------------------------------------------
[  6] local 10.0.2.3 port 5002 connected with 10.0.1.3 port 35648
[ ID] Interval            Transfer     Bandwidth        Jitter   Lost/Total  Latency avg/min/max/stdev PPS  NetPwr
[  6] 0.0000-1.0000 sec   271 KBytes  2.22 Mbits/sec   1.790 ms   80/  269 (30%)  2.122/ 0.962/10.594/ 2.084 ms  188 pps  130.92
[  6] 1.0000-2.0000 sec   284 KBytes  2.33 Mbits/sec   1.798 ms   71/  269 (26%) 10.407/ 0.983/24.850/ 6.282 ms  197 pps  27.97
[  6] 2.0000-3.0000 sec   289 KBytes  2.36 Mbits/sec   1.010 ms   64/  265 (24%)  5.671/ 0.992/14.058/ 3.373 ms  202 pps  52.10
[  6] 3.0000-4.0000 sec   274 KBytes  2.25 Mbits/sec   0.542 ms   78/  269 (29%)  1.589/ 0.986/ 6.561/ 1.015 ms  190 pps  176.66
[  6] 4.0000-5.0000 sec   254 KBytes  2.08 Mbits/sec   0.760 ms   90/  267 (34%)  1.909/ 0.973/ 7.573/ 1.274 ms  177 pps  136.32
[  6] 5.0000-6.0000 sec   281 KBytes  2.30 Mbits/sec   0.558 ms   72/  268 (27%)  1.994/ 0.970/ 7.126/ 1.312 ms  196 pps  144.53
[  6] 6.0000-7.0000 sec   256 KBytes  2.09 Mbits/sec   0.884 ms   91/  269 (34%)  1.816/ 0.966/ 6.789/ 1.232 ms  177 pps  144.09
[  6] 7.0000-8.0000 sec   281 KBytes  2.30 Mbits/sec   1.191 ms   66/  262 (25%)  3.440/ 0.979/15.187/ 3.243 ms  198 pps  83.76
[  6] 8.0000-9.0000 sec   291 KBytes  2.39 Mbits/sec   0.606 ms   68/  271 (25%)  3.249/ 0.998/14.342/ 2.972 ms  203 pps  91.84
[  6] 9.0000-10.0000 sec   280 KBytes  2.29 Mbits/sec  32.097 ms   74/  269 (28%)  4.313/ 0.954/508.708/36.323 ms  130 pps  66.46
[  6] 0.0000-10.5074 sec  2.70 MBytes  2.15 Mbits/sec  32.097 ms  754/ 2678 (28%)  3.690/ 0.954/508.708/12.167 ms  183 pps  72.95
