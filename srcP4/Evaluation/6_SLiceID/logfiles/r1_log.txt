------------------------------------------------------------
Server listening on UDP port 5000 with pid 436312
Receiving 1470 byte datagrams
UDP buffer size:  208 KByte (default)
------------------------------------------------------------
[  6] local 10.0.2.1 port 5000 connected with 10.0.1.1 port 35376
[ ID] Interval            Transfer     Bandwidth        Jitter   Lost/Total  Latency avg/min/max/stdev PPS  NetPwr
[  6] 0.0000-1.0000 sec   244 KBytes  2.00 Mbits/sec   1.456 ms    0/  170 (0%) 23.551/ 0.827/50.550/13.329 ms  169 pps  10.61
[  6] 1.0000-2.0000 sec   240 KBytes  1.96 Mbits/sec   1.373 ms    0/  167 (0%) 88.765/50.550/112.228/15.840 ms  167 pps  2.77
[  6] 2.0000-3.0000 sec   251 KBytes  2.06 Mbits/sec   1.519 ms    0/  175 (0%) 124.373/110.241/133.175/ 4.612 ms  175 pps  2.07
[  6] 3.0000-4.0000 sec   260 KBytes  2.13 Mbits/sec   1.312 ms    0/  181 (0%) 133.576/118.536/146.953/ 6.901 ms  180 pps  1.99
[  6] 4.0000-5.0000 sec   254 KBytes  2.08 Mbits/sec   1.480 ms    0/  177 (0%) 129.550/114.103/142.677/ 6.530 ms  178 pps  2.01
[  6] 5.0000-6.0000 sec   245 KBytes  2.01 Mbits/sec   1.592 ms    0/  171 (0%) 143.391/120.901/171.192/12.173 ms  171 pps  1.75
[  6] 6.0000-7.0000 sec   241 KBytes  1.98 Mbits/sec   1.276 ms    0/  168 (0%) 201.581/164.460/223.504/14.287 ms  168 pps  1.23
[  6] 7.0000-8.0000 sec   247 KBytes  2.02 Mbits/sec   1.634 ms    0/  172 (0%) 232.876/207.053/262.054/14.100 ms  171 pps  1.09
[  6] 8.0000-9.0000 sec   241 KBytes  1.98 Mbits/sec   1.851 ms    0/  168 (0%) 287.022/251.352/317.165/16.717 ms  168 pps  0.86
[  6] 9.0000-10.0000 sec   241 KBytes  1.98 Mbits/sec   1.573 ms    0/  168 (0%) 339.827/309.956/374.971/14.878 ms  168 pps  0.73
[  6] 0.0000-10.3916 sec  2.50 MBytes  2.02 Mbits/sec   1.260 ms    0/ 1784 (0%) 177.007/ 0.827/393.094/97.328 ms  171 pps  1.43
