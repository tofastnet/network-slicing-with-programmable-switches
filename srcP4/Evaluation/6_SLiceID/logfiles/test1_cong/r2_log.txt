------------------------------------------------------------
Server listening on UDP port 5001 with pid 392121
Receiving 1470 byte datagrams
UDP buffer size:  208 KByte (default)
------------------------------------------------------------
[  6] local 10.0.2.2 port 5001 connected with 10.0.1.2 port 36899
[ ID] Interval            Transfer     Bandwidth        Jitter   Lost/Total  Latency avg/min/max/stdev PPS  NetPwr
[  6] 0.0000-1.0000 sec  53.1 KBytes   435 Kbits/sec   1.690 ms   57/   94 (61%)  3.872/ 1.378/10.007/ 2.425 ms   36 pps  14.05
[  6] 1.0000-2.0000 sec  53.1 KBytes   435 Kbits/sec   1.864 ms   52/   89 (58%)  4.047/ 1.038/12.449/ 2.890 ms   37 pps  13.44
[  6] 2.0000-3.0000 sec  44.5 KBytes   365 Kbits/sec   2.365 ms   60/   91 (66%)  4.481/ 1.215/11.393/ 2.650 ms   30 pps  10.17
[  6] 3.0000-4.0000 sec  45.9 KBytes   376 Kbits/sec   1.609 ms   54/   86 (63%)  5.721/ 1.178/15.727/ 4.593 ms   33 pps  8.22
[  6] 4.0000-5.0000 sec  47.4 KBytes   388 Kbits/sec   1.671 ms   55/   88 (62%)  4.142/ 1.280/10.187/ 2.374 ms   33 pps  11.71
[  6] 5.0000-6.0000 sec  54.6 KBytes   447 Kbits/sec   1.682 ms   52/   90 (58%)  5.488/ 1.065/14.264/ 4.183 ms   38 pps  10.18
[  6] 6.0000-7.0000 sec  67.5 KBytes   553 Kbits/sec   1.479 ms   41/   88 (47%)  4.551/ 1.077/16.122/ 4.075 ms   47 pps  15.18
[  6] 7.0000-8.0000 sec  48.8 KBytes   400 Kbits/sec   1.410 ms   56/   90 (62%)  6.085/ 1.067/17.694/ 5.486 ms   34 pps  8.21
[  6] 8.0000-9.0000 sec  54.6 KBytes   447 Kbits/sec   1.375 ms   50/   88 (57%)  3.402/ 1.088/ 8.670/ 2.123 ms   38 pps  16.42
[  6] 0.0000-9.9826 sec   525 KBytes   431 Kbits/sec   1.350 ms  526/  892 (59%)  4.422/ 1.038/17.694/ 3.506 ms   36 pps  12.19
