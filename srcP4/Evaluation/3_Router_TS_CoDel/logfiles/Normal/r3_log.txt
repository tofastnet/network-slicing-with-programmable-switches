------------------------------------------------------------
Server listening on UDP port 5002 with pid 249217
Receiving 1470 byte datagrams
UDP buffer size:  208 KByte (default)
------------------------------------------------------------
[  6] local 10.0.2.3 port 5002 connected with 10.0.1.3 port 54611
[ ID] Interval            Transfer     Bandwidth        Jitter   Lost/Total  Latency avg/min/max/stdev PPS  NetPwr
[  6] 0.0000-1.0000 sec   386 KBytes  3.16 Mbits/sec   1.465 ms    0/  269 (0%)  2.650/ 0.642/ 5.643/ 1.402 ms  268 pps  149.24
[  6] 1.0000-2.0000 sec   382 KBytes  3.13 Mbits/sec   1.692 ms    0/  266 (0%)  2.772/ 0.636/ 6.049/ 1.466 ms  266 pps  141.05
[  6] 2.0000-3.0000 sec   386 KBytes  3.16 Mbits/sec   1.470 ms    0/  269 (0%)  2.988/ 0.645/ 9.087/ 1.678 ms  269 pps  132.36
[  6] 3.0000-4.0000 sec   382 KBytes  3.13 Mbits/sec   2.099 ms    0/  266 (0%)  3.231/ 0.645/ 8.949/ 1.834 ms  266 pps  121.01
[  6] 4.0000-5.0000 sec   385 KBytes  3.15 Mbits/sec   1.580 ms    0/  268 (0%)  3.656/ 0.639/11.250/ 2.031 ms  268 pps  107.74
[  6] 5.0000-6.0000 sec   385 KBytes  3.15 Mbits/sec   1.709 ms    0/  268 (0%)  2.766/ 0.645/ 7.570/ 1.537 ms  268 pps  142.41
[  6] 6.0000-7.0000 sec   385 KBytes  3.15 Mbits/sec   1.663 ms    0/  268 (0%)  3.318/ 0.656/ 8.877/ 1.736 ms  268 pps  118.75
[  6] 7.0000-8.0000 sec   385 KBytes  3.15 Mbits/sec   1.609 ms    0/  268 (0%)  3.069/ 0.641/ 7.872/ 1.619 ms  267 pps  128.37
[  6] 8.0000-9.0000 sec   383 KBytes  3.14 Mbits/sec   1.908 ms    0/  267 (0%)  3.301/ 0.618/ 9.980/ 1.870 ms  267 pps  118.91
[  6] 0.0000-9.9993 sec  3.75 MBytes  3.15 Mbits/sec   1.580 ms    0/ 2676 (0%)  3.034/ 0.618/11.250/ 1.696 ms  267 pps  129.67
