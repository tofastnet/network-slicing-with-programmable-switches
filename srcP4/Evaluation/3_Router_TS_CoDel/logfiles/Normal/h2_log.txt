------------------------------------------------------------
Client connecting to 10.0.2.2, UDP port 5001 with pid 249264
Sending 1470 byte datagrams, IPG target: 11215.21 us (kalman adjust)
UDP buffer size:  208 KByte (default)
------------------------------------------------------------
[  6] local 10.0.1.2 port 36045 connected with 10.0.2.2 port 5001
[ ID] Interval            Transfer     Bandwidth      Write/Err  PPS
[  6] 0.0000-1.0000 sec   131 KBytes  1.07 Mbits/sec  91/0       90 pps
[  6] 1.0000-2.0000 sec   128 KBytes  1.05 Mbits/sec  89/0       89 pps
[  6] 2.0000-3.0000 sec   128 KBytes  1.05 Mbits/sec  89/0       89 pps
[  6] 3.0000-4.0000 sec   128 KBytes  1.05 Mbits/sec  89/0       89 pps
[  6] 4.0000-5.0000 sec   128 KBytes  1.05 Mbits/sec  89/0       89 pps
[  6] 5.0000-6.0000 sec   129 KBytes  1.06 Mbits/sec  90/0       89 pps
[  6] 6.0000-7.0000 sec   128 KBytes  1.05 Mbits/sec  89/0       89 pps
[  6] 7.0000-8.0000 sec   128 KBytes  1.05 Mbits/sec  89/0       89 pps
[  6] 8.0000-9.0000 sec   128 KBytes  1.05 Mbits/sec  89/0       89 pps
[  6] 0.0000-10.0045 sec  1.25 MBytes  1.05 Mbits/sec  892/0       89 pps
[  6] Sent 892 datagrams
[  6] Server Report:
[  6] 0.0000-10.0060 sec  1.25 MBytes  1.05 Mbits/sec   2.793 ms    0/  892 (0%)  2.668/ 0.642/ 9.831/ 1.723 ms   89 pps  49.12
