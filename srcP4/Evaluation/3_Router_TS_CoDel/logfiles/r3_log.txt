------------------------------------------------------------
Server listening on UDP port 5002 with pid 320771
Receiving 1470 byte datagrams
UDP buffer size:  208 KByte (default)
------------------------------------------------------------
[  6] local 10.0.2.3 port 5002 connected with 10.0.1.3 port 45635
[ ID] Interval            Transfer     Bandwidth        Jitter   Lost/Total  Latency avg/min/max/stdev PPS  NetPwr
[  6] 0.0000-1.0000 sec   303 KBytes  2.48 Mbits/sec   1.773 ms    4/  215 (1.9%) 108.174/ 3.996/204.505/58.370 ms  210 pps  2.87
[  6] 1.0000-2.0000 sec   297 KBytes  2.43 Mbits/sec   1.718 ms   19/  226 (8.4%) 285.452/204.505/356.927/43.481 ms  207 pps  1.07
[  6] 2.0000-3.0000 sec   291 KBytes  2.39 Mbits/sec   2.070 ms   38/  241 (16%) 413.091/356.691/459.352/28.507 ms  203 pps  0.72
[  6] 3.0000-4.0000 sec   294 KBytes  2.41 Mbits/sec   1.436 ms   51/  256 (20%) 486.332/458.982/502.769/11.453 ms  205 pps  0.62
[  6] 0.0000-4.5221 sec  1.31 MBytes  2.44 Mbits/sec   2.017 ms  136/ 1073 (13%) 342.690/ 3.996/523.573/152.547 ms  207 pps  0.89
