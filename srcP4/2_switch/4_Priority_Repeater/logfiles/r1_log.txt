------------------------------------------------------------
Server listening on UDP port 5000 with pid 231273
Receiving 1470 byte datagrams
UDP buffer size:  208 KByte (default)
------------------------------------------------------------
[  6] local 10.0.2.1 port 5000 connected with 10.0.1.1 port 40911
[ ID] Interval            Transfer     Bandwidth        Jitter   Lost/Total  Latency avg/min/max/stdev PPS  NetPwr
[  6] 0.0000-1.0000 sec   202 KBytes  1.66 Mbits/sec   2.174 ms    0/  141 (0%) 107.316/ 2.050/215.073/62.838 ms  140 pps  1.93
[  6] 1.0000-2.0000 sec   201 KBytes  1.65 Mbits/sec   2.301 ms    0/  140 (0%) 324.328/214.143/426.453/62.048 ms  140 pps  0.63
[  6] 2.0000-3.0000 sec   248 KBytes  2.03 Mbits/sec   3.640 ms    0/  173 (0%) 469.533/426.453/505.281/15.389 ms  171 pps  0.54
[  6] 3.0000-4.0000 sec   244 KBytes  2.00 Mbits/sec   4.778 ms    0/  170 (0%) 481.445/448.786/520.687/15.646 ms  172 pps  0.52
[  6] 4.0000-5.0000 sec   261 KBytes  2.14 Mbits/sec   4.173 ms    0/  182 (0%) 476.861/448.214/505.128/13.014 ms  182 pps  0.56
[  6] 5.0000-6.0000 sec   260 KBytes  2.13 Mbits/sec   3.525 ms    0/  181 (0%) 471.041/446.356/500.977/11.029 ms  180 pps  0.56
[  6] 6.0000-7.0000 sec   258 KBytes  2.12 Mbits/sec   4.104 ms    0/  180 (0%) 474.038/448.508/503.186/10.760 ms  179 pps  0.56
[  6] 7.0000-8.0000 sec   254 KBytes  2.08 Mbits/sec   2.612 ms    0/  177 (0%) 474.547/448.457/504.242/10.350 ms  179 pps  0.55
[  6] 8.0000-9.0000 sec   253 KBytes  2.07 Mbits/sec   2.946 ms    0/  176 (0%) 470.737/447.529/484.234/ 9.053 ms  176 pps  0.55
[  6] 9.0000-10.0000 sec   257 KBytes  2.11 Mbits/sec   3.906 ms    0/  179 (0%) 472.309/444.792/504.066/11.881 ms  178 pps  0.56
[  6] 0.0000-10.4984 sec  2.50 MBytes  2.00 Mbits/sec   4.111 ms    0/ 1784 (0%) 431.203/ 2.050/520.687/106.554 ms  169 pps  0.58
