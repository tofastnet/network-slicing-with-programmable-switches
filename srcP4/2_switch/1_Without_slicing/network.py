from p4utils.mininetlib.network_API import NetworkAPI

net = NetworkAPI()

net.setLogLevel('info')

#Switches
net.addP4Switch('s1', cli_input='s1-commands.txt')
net.setThriftPort('s1',9080)
net.addP4Switch('s2', cli_input='s2-commands.txt')
net.setThriftPort('s2',9090)

net.setP4Source('s1','codel.p4')
net.setP4Source('s2','codel.p4')

#Hosts
net.addHost('h1')
net.addHost('h2')
net.addHost('h3')

net.addHost('r1')
net.addHost('r2')
net.addHost('r3')

#Links
net.addLink('s1','h1')
net.addLink('s1','h2')
net.addLink('s1','h3')

net.addLink('s1','s2')
net.setBw('s1','s2',5)

net.addLink('s2','r1')
net.addLink('s2','r2')
net.addLink('s2','r3')


# Ports
net.setIntfPort('h1','s1',0)
net.setIntfPort('h2','s1',0)
net.setIntfPort('h3','s1',0)

net.setIntfPort('s1','h1',1)
net.setIntfPort('s1','h2',2)
net.setIntfPort('s1','h3',3)

net.setIntfPort('s1','s2',4)
net.setIntfPort('s2','s1',4)

net.setIntfPort('s2','r1',1)
net.setIntfPort('s2','r2',2)
net.setIntfPort('s2','r3',3)

net.setIntfPort('r1','s2',0)
net.setIntfPort('r2','s2',0)
net.setIntfPort('r3','s2',0)

# Names
net.setIntfName('h1','s1','h1-eth0')
net.setIntfName('h2','s1','h2-eth0')
net.setIntfName('h3','s1','h3-eth0')

net.setIntfName('s1','h1','s1-eth1')
net.setIntfName('s1','h2','s1-eth2')
net.setIntfName('s1','h3','s1-eth3')

net.setIntfName('s1','s2','s1-eth4')
net.setIntfName('s2','s1','s2-eth4')

net.setIntfName('s2','r1','s2-eth1')
net.setIntfName('s2','r2','s2-eth2')
net.setIntfName('s2','r3','s2-eth3')

net.setIntfName('r1','s2','r1-eth0')
net.setIntfName('r2','s2','r2-eth0')
net.setIntfName('r3','s2','r3-eth0')

# IPs
net.setIntfIp('h1','s1','10.0.1.1/16')
net.setIntfIp('h2','s1','10.0.1.2/16')
net.setIntfIp('h3','s1','10.0.1.3/16')

net.setIntfIp('r1','s2','10.0.2.1/16')
net.setIntfIp('r2','s2','10.0.2.2/16')
net.setIntfIp('r3','s2','10.0.2.3/16')

# MACs
net.setIntfMac('h1','s1','00:00:0a:00:01:01')
net.setIntfMac('h2','s1','00:00:0a:00:01:02')
net.setIntfMac('h3','s1','00:00:0a:00:01:03')

net.setIntfMac('s2','s1','00:00:00:00:02:01')
net.setIntfMac('s1','s2','00:00:00:00:01:02')

net.setIntfMac('r1','s2','00:00:0a:00:02:01')
net.setIntfMac('r2','s2','00:00:0a:00:02:02')
net.setIntfMac('r3','s2','00:00:0a:00:02:03')


#net.mixed()
net.enablePcapDumpAll()
net.enableLogAll()

net.enableCli()
net.addTaskFile('tasks.txt')
net.startNetwork()
