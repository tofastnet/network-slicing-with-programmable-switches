import sys
import random
import time
from p4utils.utils.helper import load_topo
from subprocess import Popen

topo = load_topo('topology.json')

send_cmds = []
recv_cmds = []

Popen("sudo killall iperf iperf3", shell=True)

for i in range(1,4):
	Bitrate = 0
	if(i==1):
		Bitrate = 0.5
	elif(i==2):
		Bitrate = 1
	else:
		Bitrate = 3


	iperf_send = f"mx h{i} iperf -c 10.0.2.{i} -i 1 -t 60 -p 500{i-1} -u -b {Bitrate}M -e > ./logfiles/h{i}_log.txt"
	iperf_recv = f"mx r{i} iperf -s -e -p 500{i-1} -u -i 1 > ./logfiles/r{i}_log.txt"

	send_cmds.append(iperf_send)
	recv_cmds.append(iperf_recv)

#start receivers first
for recv_cmd in recv_cmds:
    Popen(recv_cmd, shell=True)

time.sleep(1)

for send_cmd in send_cmds:
    Popen(send_cmd, shell=True)
