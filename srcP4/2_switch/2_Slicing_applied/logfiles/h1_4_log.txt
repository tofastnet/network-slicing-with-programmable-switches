------------------------------------------------------------
Client connecting to 10.0.2.1, UDP port 5004 with pid 126154
Sending 1470 byte datagrams, IPG target: 22430.42 us (kalman adjust)
UDP buffer size:  208 KByte (default)
------------------------------------------------------------
[  6] local 10.0.1.1 port 57479 connected with 10.0.2.1 port 5004
[ ID] Interval            Transfer     Bandwidth      Write/Err  PPS
[  6] 0.0000-1.0000 sec  66.0 KBytes   541 Kbits/sec  46/4       46 pps
[  6] 1.0000-2.0000 sec  63.2 KBytes   517 Kbits/sec  44/2       45 pps
[  6] 2.0000-3.0000 sec  64.6 KBytes   529 Kbits/sec  45/0       45 pps
[  6] 3.0000-4.0000 sec  64.6 KBytes   529 Kbits/sec  45/1       45 pps
[  6] 4.0000-5.0000 sec  63.2 KBytes   517 Kbits/sec  44/1       45 pps
[  6] 0.0000-5.0256 sec   322 KBytes   524 Kbits/sec  224/8       44 pps
[  6] Sent 224 datagrams
