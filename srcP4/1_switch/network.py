from p4utils.mininetlib.network_API import NetworkAPI

net = NetworkAPI()

net.setLogLevel('info')

#Switches
net.addP4Switch('s1', cli_input='s1-commands.txt')
net.setThriftPort('s1',9080)

net.setP4Source('s1','codel.p4')

#Hosts
net.addHost('h1')
net.addHost('r1')

#Links
net.addLink('s1','h1')
net.addLink('s1','r1')


# Ports
net.setIntfPort('h1','s1',0)

net.setIntfPort('s1','h1',1)
net.setIntfPort('s1','r1',2)

net.setIntfPort('r1','s1',0)

# Names
net.setIntfName('h1','s1','h1-eth0')
net.setIntfName('s1','h1','s1-eth1')

net.setIntfName('s1','r1','s1-eth2')
net.setIntfName('r1','s1','r1-eth0')

# IPs
net.setIntfIp('h1','s1','10.0.0.1/24')
net.setIntfIp('r1','s1','10.0.0.2/24')

# MACs
net.setIntfMac('h1','s1','00:00:0a:00:00:01')
net.setIntfMac('r1','s1','00:00:0a:00:00:02')

net.setIntfMac('s1','h1','00:00:00:00:00:01')
net.setIntfMac('s1','r1','00:00:00:00:00:02')

#net.mixed()
net.enablePcapDumpAll()
net.enableLogAll()

net.enableCli()
net.startNetwork()
